.PHONY: all, clean

all:
	docker run --rm -it -v $(PWD):/build gitlab.beilich.de:4567/docker-utilities/pandoc pandoc -H header.tex --template mod_template.tex -f markdown -t beamer presentation.md -o presentation.pdf
	docker run --rm -it -v $(PWD):/build gitlab.beilich.de:4567/docker-utilities/pandoc pandoc -H header.tex --template mod_template.tex -f markdown -t beamer presentation_part_two.md -o presentation_part_two.pdf

clean:
	rm -f *.pdf
