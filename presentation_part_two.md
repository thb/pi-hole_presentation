---
author: 'Wesam Al-Shaibani, Robert Beilich, Peter Neuhoff'
title: 'Pi-hole - Further into the event horizon'
...
#
\titlepage

# News: Google und Ad-blocker
<!-- ask them what they think I'm going to tell them -->
# News: Google und Ad-blocker
- Chrome Manifest V3[^1] ersetzt Grundlage vieler Ad-blocker
  - bösartigen Extensions Zugriff auf gesamten Traffic verbieten

\ 

- interne Tests, um Performance von Adblockern zu gewährleisten
- Begrenzung auf 30k Regeln pro Extension
  - EasyList hat allein schon 40k Regeln

[^1]: <https://9to5google.com/2019/06/13/google-creates-chrome-ad-blocker-extension/>

# News: Google und Ad-blocker

- Chrome Marktanteil von 70%[^2] (Stand Juni 2019)
- pihole ist davon unabhängig

[^2]: <https://de.statista.com/statistik/daten/studie/157944/umfrage/marktanteile-der-browser-bei-der-internetnutzung-weltweit-seit-2009/>

# OpenWRT und Pihole

- OpenWRT: Open Source Router Betriebssystem
- Pihole ist inkompatible mit OpenWRT

# OpenWRT Adblock

- eigenes Modul zum Blocken von Werbung[^3]
- basiert wie auch Pihole auf dnsmasq

\ 

- Pihole hat darüber hinaus
  - einen verbessertes dnsmasq: FTLDNS[^4]
  - eine bessere Oberfläche (Statistiken, Maintenance)

[^3]: <https://openwrt.org/docs/guide-user/services/ad-blocking>
[^4]: <https://pi-hole.net/2018/02/22/coming-soon-ftldns-pi-holes-own-dns-dhcp-server/>

# dnsmasq und FTLDNS

- "FTLDNS is dnsmasq blended with Pi-hole’s special sauce"
- keine Kompatibilitätsprobleme mit vorinstalliertem dnsmasq und Pihole
- keine großen Codeveränderungen an dnsmasq -> Updates können eingespielt werden

\ 

- Regexbasiertes Matching von URLs[^5]

[^5]: <https://pi-hole.net/2018/04/24/blocking-via-regex-now-available-in-ftldns/>
