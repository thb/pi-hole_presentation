---
author: 'Wesam Al-Shaibani, Robert Beilich, Peter Neuhoff'
title: 'Pi-hole - A black hole for Internet advertisements'
...
# Spot the difference {.plain}
<!-- Spot the difference with ad-->
\vspace*{\fill}
\noindent
\makebox[\textwidth]{\includegraphics[width=\paperwidth]{./graphics/spot_the_difference_ad.png}}

\vspace*{2cm}

# Spot the difference {.plain}
<!-- Spot the difference with analytics -->
\vspace*{\fill}
\noindent
\makebox[\textwidth]{\includegraphics[width=\paperwidth]{./graphics/spot_the_difference_analytics.png}}

# Spot the difference - Result {.plain}
<!-- Spot the difference with analytics: result -->
\vspace*{\fill}
\noindent
\makebox[\textwidth]{\includegraphics[width=\paperwidth]{./graphics/difference_analytics.png}}

\vspace*{2cm}
 
#
\titlepage

# Pi-hole - A black hole for Internet advertisements
\colA{6cm}
![](./graphics/pi-hole.png){width=100%}\ 
\colB{6cm}
\begin{itemize}
\item Initial release: 15 Juni 2015
\item Bash (web dashboard in PHP)
\item noch immer aktiv in Entwicklung
\item minimale Anforderungen: 250MB RAM, ~52MB Speicher
\item für Debian (und Derivate) entwickelt
\end{itemize}

\begin{itemize}
\item blockt Werbung und Tracker
\end{itemize}
\colEnd

# Technische Grundlage

**Pi-hole dient als DNS-Proxy mit Blacklists**

## Quick reminder
DNS = Domain Name System
DNS dient zur Auflösung von Domain Namen zu IPs
th-brandenburg.de &rarr; 195.37.0.210

Proxy = Vertreter/"Zwischenhändler"
Es wird immer noch ein DNS Server benötigt den Pi-hole anfragt

# Limitations
Werbung die vom Seitenbetreiber selbst geschaltet wird kann nicht geblockt werden

nur auf DNS Ebene &rarr; kein Blocken von IPs

Abhängig von den Bereitstellern der Blocklisten

Probleme für nicht technik-afine Nutzer des Netzwerkes

# Pi-hole und Adblocker

es gibt auch außerhalb von Browsern Werbung/Tracker

nicht alle Geräte sind mit einem Adblocker bestückbar

- Smart-Tvs
- Konsolen
- IoT-Devices
- (Smartphones)

# Pi-hole und Adblocker

Adblocker können auch Werbung der gleichen Seite blocken

Adblocker lassen sich für bestimmte Seiten abstellen

Adblocker sind unabhängig vom Netzwerk

manche Adblocker sind nur kosmetisch &rarr; keine Privatsphäre

# Personalisierung ...
## ... ist nicht schlecht ...
- Autovervollständigung im Kontext
- Vorschläge (bspw. Videos auf Youtube oder Suchvorschläge)
- Werbung

# Personalisierung ...
## ... aber nur wenn sie gut gemacht ist

- Werbung für Telefonverträge für Neukunden an bestehende Kunden senden
- Möchten Sie vielleicht Pommes zu den Pommes: <https://www.youtube.com/watch?v=6-TpRmQtVEI>

#
\vspace*{\fill}
\noindent
\makebox[\textwidth]{\includegraphics[width=\paperwidth]{./graphics/DadTargetedAd.png}}

\vspace*{2cm}


#
\centeredPage{Demo --- Pi-hole}

# Setup
```
wget -O basic-install.sh https://install.pi-hole.net
sudo bash basic-install.sh
```

- im Normalfall Standardeinstellungen
- DNS-Server nach eigenem Belieben
- FTL logging Level --- Convenience vs Privacy

# mobiler Einsatz

Repo: <https://gitlab.beilich.de/thb/pi-hole>

- nat-setup.sh: auf dem (Linux-)Rechner
- pi-setup.sh: auf dem pi

# Quellen
- Pi-hole: <https://pi-hole.net>
- Targeted ad dad: <https://www.zaius.com/wp-content/uploads/2017/08/DadTargetedAd.png>
